package mycat.multi.tenancy.service.impl;

import lombok.extern.slf4j.Slf4j;
import mycat.multi.tenancy.domain.Business;
import mycat.multi.tenancy.mapper.BusinessMapper;
import mycat.multi.tenancy.service.BusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 开发公司：联信
 * 版权：联信
 * <p>
 * Annotation
 *
 * @author 刘志强
 * @created Create Time: 2021/1/26
 */
@Service
@Slf4j
public class BusinessServiceImpl implements BusinessService {

    @Autowired
    private BusinessMapper businessMapper;

    @Override
    public Business getBusiness(Long id) {
        return businessMapper.getBusiness(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Business addBusiness(Business business) {
        businessMapper.addBusiness(business);
        return business;
    }
}
