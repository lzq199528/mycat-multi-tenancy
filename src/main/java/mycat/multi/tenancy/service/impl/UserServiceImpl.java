package mycat.multi.tenancy.service.impl;

import mycat.multi.tenancy.domain.UserInfo;
import mycat.multi.tenancy.mapper.UserMapper;
import mycat.multi.tenancy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 开发公司：联信
 * 版权：联信
 * <p>
 * Annotation
 *
 * @author 刘志强
 * @created Create Time: 2021/1/26
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    public static final Map<String, UserInfo> map = new ConcurrentHashMap<>(15);

    @Override
    public String login(String userName, String password) {
        UserInfo userInfo = userMapper.getUserByUserNameAndPassword(userName,password);
        if (userInfo != null) {
            String token = UUID.randomUUID().toString();
            map.put(token, userInfo);
            return token;
        } else {
            return "无此用户";
        }
    }
}
