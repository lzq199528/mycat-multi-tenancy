package mycat.multi.tenancy.service;

/**
 * 开发公司：联信
 * 版权：联信
 * <p>
 * Annotation
 *
 * @author 刘志强
 * @created Create Time: 2021/1/26
 */
public interface UserService {


    /**
     * 登录
     * @param userName
     * @param password
     * @return
     */
    String login(String userName, String password);
}
