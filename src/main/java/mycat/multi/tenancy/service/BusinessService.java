package mycat.multi.tenancy.service;


import mycat.multi.tenancy.domain.Business;

/**
 * 开发公司：联信
 * 版权：联信
 * <p>
 * Annotation
 *
 * @author 刘志强
 * @created Create Time: 2021/1/26
 */
public interface BusinessService {

    /**
     * 获取业务信息
     * @param id
     * @return
     */
    Business getBusiness(Long id);

    /**
     * 事务测试
     * @param business
     * @return
     */
    Business addBusiness(Business business);
}
