package mycat.multi.tenancy.controller;


import lombok.extern.slf4j.Slf4j;
import mycat.multi.tenancy.domain.Business;
import mycat.multi.tenancy.service.BusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 开发公司：联信
 * 版权：联信
 * <p>
 * Annotation
 *
 * @author 刘志强
 * @created Create Time: 2021/1/26
 */
@RestController
@RequestMapping("business")
@CrossOrigin
@Slf4j
public class BusinessController {

    @Autowired
    private BusinessService businessService;

    @GetMapping("getBusiness")
    public Business getBusiness(Long id) {
        return businessService.getBusiness(id);
    }

    @PostMapping("addBusiness")
    public Business addBusiness(Business business) {
        return businessService.addBusiness(business);
    }
}
