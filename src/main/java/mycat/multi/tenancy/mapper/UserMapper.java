package mycat.multi.tenancy.mapper;

import mycat.multi.tenancy.domain.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 开发公司：联信
 * 版权：联信
 * <p>
 * Annotation
 *
 * @author 刘志强
 * @created Create Time: 2021/1/26
 */
@Mapper
public interface UserMapper {
    /**
     * 根据用户名和密码查找用户
     * @param userName
     * @param password
     * @return
     */
    UserInfo getUserByUserNameAndPassword(
            @Param("userName")
            String userName,
            @Param("password")
            String password);
}
