package mycat.multi.tenancy.mapper;

import mycat.multi.tenancy.domain.Business;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 开发公司：联信
 * 版权：联信
 * <p>
 * Annotation
 *
 * @author 刘志强
 * @created Create Time: 2021/1/26
 */
@Mapper
public interface BusinessMapper {

    /**
     * 获取业务详情
     * @param id
     * @return
     */
    Business getBusiness(@Param("id") Long id);

    /**
     * 添加
     * @param business
     */
    void addBusiness(Business business);
}
