package mycat.multi.tenancy.domain;

import lombok.Data;

/**
 * 开发公司：联信
 * 版权：联信
 * <p>
 * Annotation
 *
 * @author 刘志强
 * @created Create Time: 2021/1/26
 */
@Data
public class UserInfo {

    private Long id;

    private String userName;

    private String password;

    private Long tenantId;

    private String tenantName;

    private String tenantDatabase;

    private String tenantDatabaseUrl;

    private String tenantDatabaseName;

    private String tenantDatabasePassword;

    @Override
    public String toString() {
        return "UserInfo{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", tenantId=" + tenantId +
                ", tenantName='" + tenantName + '\'' +
                ", tenantDatabase='" + tenantDatabase + '\'' +
                ", tenantDatabaseUrl='" + tenantDatabaseUrl + '\'' +
                ", tenantDatabaseName='" + tenantDatabaseName + '\'' +
                ", tenantDatabasePassword='" + tenantDatabasePassword + '\'' +
                '}';
    }
}
