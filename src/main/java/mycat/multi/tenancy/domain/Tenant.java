package mycat.multi.tenancy.domain;

import lombok.Data;

/**
 * 开发公司：联信
 * 版权：联信
 * <p>
 * Annotation
 *
 * @author 刘志强
 * @created Create Time: 2021/2/3
 */
@Data
public class Tenant {

    private Long id;

    private String tenantName;

    private String tenantDatabase;

    private String tenantDatabaseUrl;

    private String tenantDatabaseName;

    private String tenantDatabasePassword;

    @Override
    public String toString() {
        return "Tenant{" +
                "id=" + id +
                ", tenantName='" + tenantName + '\'' +
                ", tenantDatabase='" + tenantDatabase + '\'' +
                ", tenantDatabaseUrl='" + tenantDatabaseUrl + '\'' +
                ", tenantDatabaseName='" + tenantDatabaseName + '\'' +
                ", tenantDatabasePassword='" + tenantDatabasePassword + '\'' +
                '}';
    }
}
