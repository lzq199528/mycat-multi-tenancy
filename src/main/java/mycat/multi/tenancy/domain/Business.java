package mycat.multi.tenancy.domain;

import lombok.Data;

/**
 * 开发公司：联信
 * 版权：联信
 * <p>
 * Annotation
 *
 * @author 刘志强
 * @created Create Time: 2021/1/26
 */
@Data
public class Business {

    private Long id;

    private String businessFieldOne;

    private String businessFieldTwo;

    private String businessFieldThree;

    private String businessFieldFour;

}
