/*
 Navicat Premium Data Transfer

 Source Server         : 101.37.152.195
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : 101.37.152.195:3306
 Source Schema         : tenant_wy

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 26/01/2021 15:31:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for business
-- ----------------------------
DROP TABLE IF EXISTS `business`;
CREATE TABLE `business`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `business_field_one` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '业务字段1',
  `business_field_two` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '业务字段1',
  `business_field_three` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '业务字段1',
  `business_field_four` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '业务字段1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of business
-- ----------------------------
INSERT INTO `business` VALUES (1, '2', '2', '2', '2');
INSERT INTO `business` VALUES (4, 'aa', 'aa', 'aa', 'aa');

SET FOREIGN_KEY_CHECKS = 1;
